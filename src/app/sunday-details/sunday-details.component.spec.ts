import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SundayDetailsComponent } from './sunday-details.component';

describe('SundayDetailsComponent', () => {
  let component: SundayDetailsComponent;
  let fixture: ComponentFixture<SundayDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SundayDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SundayDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
