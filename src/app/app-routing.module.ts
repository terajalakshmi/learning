import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { SaturdayDetailsComponent } from './saturday-details/saturday-details.component';
import { SundayDetailsComponent } from './sunday-details/sunday-details.component';
import { ChartdetailComponent } from './chartdetail/chartdetail.component';

const routes: Routes = [ 
 { path: 'saturday', component: SaturdayDetailsComponent },
 { path: 'sunday', component: SundayDetailsComponent},
 { path:  'chart', component: ChartdetailComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
