import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaturdayDetailsComponent } from './saturday-details.component';

describe('SaturdayDetailsComponent', () => {
  let component: SaturdayDetailsComponent;
  let fixture: ComponentFixture<SaturdayDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaturdayDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaturdayDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
