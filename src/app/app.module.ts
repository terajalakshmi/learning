import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SaturdayDetailsComponent } from './saturday-details/saturday-details.component';
import { SundayDetailsComponent } from './sunday-details/sunday-details.component';
import { ChartdetailComponent } from './chartdetail/chartdetail.component';
import { ChartsModule } from 'ng2-charts';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    SaturdayDetailsComponent,
    SundayDetailsComponent,
    ChartdetailComponent, 
  ],

     imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    FormsModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
