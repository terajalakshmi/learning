import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartdetailComponent } from './chartdetail.component';

describe('ChartdetailComponent', () => {
  let component: ChartdetailComponent;
  let fixture: ComponentFixture<ChartdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
