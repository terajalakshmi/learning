import { Component, OnInit } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { ChartOptions, ChartType, ChartDataSets  } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-chartdetail',
  templateUrl: './chartdetail.component.html',
  styleUrls: ['./chartdetail.component.css']
})
export class ChartdetailComponent implements OnInit {
  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: Label[] = ['Sunday', 'Saturday'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: [65,80], label: 'Series A' },
    { data: [28,70], label: 'Series B' }
  ];
    

  constructor() { }

  ngOnInit(): void {
  }

}
